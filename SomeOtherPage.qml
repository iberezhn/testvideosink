import QtQuick
import QtQuick.Window
import QtQuick.Controls
import QtQuick.Layouts
import QtMultimedia

Window {
    width: 640
    height: 480
    visible: true
    color: 'white'
    title: qsTr("Hello World")

    Button {
        width: parent.width /2
        height: 120
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors {
            leftMargin: 16
            rightMargin: 16
        }
        text: "START"
        onClicked: {
            console.log("START!")

            pageLoader.setSource("qrc:/pages/main.qml")
        }
    }
}
