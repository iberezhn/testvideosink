#include <QtWidgets/QApplication>
#include <QQmlApplicationEngine>
#include <QPermission>

#include "producer.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    qmlRegisterType<Producer>("io.qt.Producer", 1, 0, "Producer");

#if QT_CONFIG(permissions)
    app.requestPermission(QCameraPermission{}, [](const QPermission &permission) {
        qInfo() << "requestPermission: QCameraPermission";
    });
    app.requestPermission(QMicrophonePermission{}, [](const QPermission &permission) {
        qInfo() << "requestPermission: QMicrophonePermission";
    });
#endif

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/pages/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
