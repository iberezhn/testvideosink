import QtQuick
import QtQuick.Window
import QtQuick.Controls
import QtQuick.Layouts
import QtMultimedia

import io.qt.Producer 1.0

Window {
    width: 640
    height: 480
    visible: true
    color: 'transparent'
    title: qsTr("Hello World")

    Loader {id: pageLoader;}

    Item {
        id : cameraUI
        width: parent.width
        height: parent.height - 120
        anchors.margins: 120

        Producer {
            id: customVideoSink
            videoSink: viewfinder.videoSink
        }
        MediaDevices {
            id: mediaDevices
        }

        CaptureSession {
            id: captureSession
            camera: Camera {
                id: camera
                cameraFormat: customVideoSink.getCameraFormat()
                cameraDevice: mediaDevices.videoInputs[1]
                onErrorStringChanged: {
                    console.log("onErrorStringChanged: ", errorString)
                }
            }
            imageCapture: ImageCapture {
                id: imageCapture
            }

            videoOutput: viewfinder

            Component.onCompleted: {
                camera.start()
            }
        }

        VideoOutput {
            id: viewfinder
            visible: true
            anchors.fill: parent
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            fillMode: VideoOutput.PreserveAspectCrop
        }
    }

    Button {
        width: parent.width /2
        height: 120
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors {
            leftMargin: 16
            rightMargin: 16
        }
        text: "STOP"
        onClicked: {
            console.log("STOP!")
            captureSession.camera.stop()
            captureSession.videoOutput = viewfinder
            customVideoSink.clearData()

            pageLoader.setSource("qrc:/pages/SomeOtherPage.qml")
        }
    }

//    Button {
//        width: parent.width /2
//        height: 120
//        anchors.bottom: parent.bottom
//        anchors.right: parent.right
//        anchors {
//            leftMargin: 16
//            rightMargin: 16
//        }
//        text: "START"
//        onClicked: {
//            console.log("START!")
//            captureSession.camera.start()
//            customVideoSink.videoSink = viewfinder.videoSink
//        }
//    }

}
