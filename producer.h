#ifndef PRODUCER_H
#define PRODUCER_H

#include <QObject>
#include <QPointer>
#include <QVideoSink>
#include <QQmlEngine>
#include <QTimer>
#include <QMutex>

#include <QRectF>
#include <QPoint>

#include <QCameraFormat>

#include <QtQml/qqmlregistration.h>

class Producer : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVideoSink* videoSink READ videoSink WRITE setVideoSink NOTIFY videoSinkChanged)

public:
    Producer(QObject *parent = nullptr);
    QVideoSink *videoSink() const;
    void setVideoSink(QVideoSink *newVideoSink);

    void processFrame(QImage *frame);

    Q_INVOKABLE bool clearData();
    Q_INVOKABLE QCameraFormat getCameraFormat() const;

private:
    mutable QMutex m_mutexFrame;
    mutable QMutex m_mutexResult;
    mutable QMutex m_mutexSink;

    QVideoSink *m_videoSink;

signals:
    void videoSinkChanged();

private slots:
    void processVideoFrame(const QVideoFrame &frame);
};
#endif // PRODUCER_H
