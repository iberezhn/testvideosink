#include "producer.h"

#include <QImage>
#include <QPainter>
#include <QSize>
#include <QVideoFrame>

#include <QRandomGenerator>
#include <QDateTime>
#include <QThread>

#include <QDir>
#include <QStandardPaths>
#include <QProcess>

#include <QMediaDevices>
#include <QDebug>
#ifdef __ANDROID__
#include <QCoreApplication>
#include <QJniObject>
#endif


Q_INVOKABLE QCameraFormat Producer::getCameraFormat() const
{
    auto formats = QMediaDevices::videoInputs()[1].videoFormats();
//    for (const auto &format : formats) {
//        qDebug() << format.resolution();
//    }
    for (const auto &format : formats) {
        if (format.resolution() == QSize(720, 480) && format.pixelFormat() == QVideoFrameFormat::Format_NV21) { // or Format_YV12
            return format;
        }
    }
    return QCameraFormat();
}

Producer::Producer(QObject *parent):QObject(parent)
{
}

QVideoSink *Producer::videoSink() const
{
    return m_videoSink;
}

void Producer::processFrame(QImage *frame)
{
    qInfo() << "processFrame -> " << "start";

    qInfo() << "processFrame -> " << "end";
}

void Producer::setVideoSink(QVideoSink *newVideoSink)
{
    Q_CHECK_PTR(newVideoSink);

    if(!newVideoSink) {
        qWarning() << "[setVideoSink] - VideoSink doesnot exist" << QThread::currentThread()->objectName() << QThread::currentThreadId();
        return;
    }
m_videoSink = nullptr;
    if (m_videoSink == newVideoSink)
        return;

    m_videoSink = newVideoSink;

    if(!m_videoSink) {
        qWarning() << "[QVideoSink] - VideoSink doesnot exist" << QThread::currentThread()->objectName() << QThread::currentThreadId();
        return;
    }
    emit videoSinkChanged();

    QObject::connect(m_videoSink, &QVideoSink::videoFrameChanged, this, &Producer::processVideoFrame, Qt::DirectConnection);
}

void Producer::processVideoFrame(const QVideoFrame &frame)
{
    qInfo() << "processVideoFrame -> " << "start";

    if(!frame.isValid()) {
        qWarning() << "[QVideoSink] - VideoSink is not valid!" << QThread::currentThread()->objectName() << QThread::currentThreadId();
        return;
    }

    bool didWeMapTheFrame = false;

    qWarning() << "[QVideoSink] - Try to map frame!";

    if(!static_cast<QVideoFrame>(frame).isMapped()){
        didWeMapTheFrame = static_cast<QVideoFrame>(frame).map(QVideoFrame::ReadOnly);
    }

    qWarning() << "[QVideoSink] - Check if frame is mapped!";

    if(static_cast<QVideoFrame>(frame).isMapped()){
        qWarning() << "[QVideoSink] - Convert QVideoFrame to QImage!";

        auto img = static_cast<QVideoFrame>(frame).toImage();

        qWarning() << "[QVideoSink] - Check if image is not Null!";

        if(!img.isNull()){
            qWarning() << "[QVideoSink] - Process image!";

            processFrame(&img);
        }
        qWarning() << "[QVideoSink] - Unmap the frame!";

        if(didWeMapTheFrame) static_cast<QVideoFrame>(frame).unmap();
    }

    qInfo() << "processVideoFrame -> " << "end";
}

Q_INVOKABLE bool Producer::clearData()
{
    if(m_videoSink) {
        QObject::disconnect(m_videoSink, &QVideoSink::videoFrameChanged, this, &Producer::processVideoFrame);
        m_videoSink = nullptr;
    }

    qInfo() << "[SDK] Clearing m_videoSink...";
    return true;
}
